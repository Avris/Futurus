<?php
/**
 * Avris Futurus
 * generuje na podstawie polskiego tekstu wejściowego
 * tekst zapisany ortografią zgodną z Mańifestem Jasieńskiego
 *
 * Author:   Andrzej Prusinowski
 * Website:  http://futurysci.avris.it
 * License:  CC-BY http://creativecommons.org/licenses/by/3.0/pl/
 */

namespace Avris\Futurus;

class Futurus
{
    public function __construct()
    {
        mb_internal_encoding('UTF-8');
    }

    public function futurizeWord($word)
    {
        $case = $this->figureOutCase($word);
        $word = mb_convert_case($word, MB_CASE_LOWER);

        $word = $this->handleChO($word);
        $word = $this->handleRz($word);
        $word = $this->handleSoft($word);

        return $this->restoreCase($word, $case);
    }

    protected function figureOutCase($word)
    {
        if (mb_convert_case($word, MB_CASE_UPPER) === $word) { return MB_CASE_UPPER; }
        if (mb_convert_case($word, MB_CASE_TITLE) === $word) { return MB_CASE_TITLE; }
        return MB_CASE_LOWER;
    }

    protected function restoreCase($word, $case)
    {
        return mb_convert_case($word, $case);
    }

    protected function handleChO($word)
    {
        return str_replace(['ch', 'ó'], ['h',  'u'], $word);
    }

    protected function handleRz($word)
    {
        $word = preg_replace('#([tp])rz#iu', '\1sz', $word);
        $word = preg_replace('#([^tp])rz#iu', '\1ż', $word);
        $word = preg_replace('#^rz#iu', 'ż', $word);

        return $word;
    }

    protected function handleSoft($word)
    {
        $softs = [ 'z' => 'ź', 's' => 'ś', 'n' => 'ń', 'c' => 'ć' ];
        $vowels = 'aoeiuyąę';

        foreach ($softs as $hard => $soft) {
            $word = preg_replace('#'.$hard.'i(['.$vowels.'])#iu', $soft.'\1', $word);
            $word = preg_replace('#'.$hard.'i([^'.$vowels.'])#iu', $soft.'i\1', $word);
            $word = preg_replace('#'.$hard.'i$#iu', $soft.'i', $word);
        }

        $word = preg_replace('#([fr])i(['.$vowels.'])#iu', '\1j\2', $word);

        return $word;
    }

    public function futurizeText($text)
    {
        return preg_replace_callback('#[a-ząćęłńóśźż]+#iu', array($this, 'futurizeTextCallback'), $text);
    }

    protected function futurizeTextCallback($word)
    {
        return $this->futurizeWord($word[0]);
    }
}
