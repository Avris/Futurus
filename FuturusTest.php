<?php
namespace Avris\Futurus;

class FuturusTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider        wordProvider
     */
    public function testFuturizeWord($word, $futurizedWord)
    {
        $futurus = new Futurus();
        $this->assertEquals($futurizedWord, $futurus->futurizeWord($word));

    }

    public function wordProvider()
    {
        return [
            ['ziele', 'źele'],
            ['wyrażenia', 'wyrażeńa'],
            ['różnych', 'rużnyh'],
            ['znaków', 'znakuw'],
            ['nie', 'ńe'],
            ['różnią', 'rużńą'],
            ['posiadając', 'pośadając'],
            ['sieć', 'śeć'],
            ['sposób', 'sposub'],
            ['wzór', 'wzur'],
            ['spółgłosek', 'spułgłosek'],
            ['jakich', 'jakih'],
            ['chwilą', 'hwilą'],
            ['przełamania', 'pszełamańa'],
            ['manifest', 'mańifest'],
            ['ortografii', 'ortografji'],
            ['pisowni', 'pisowńi'],
            ['niepotrzebne', 'ńepotszebne'],
            ['materiału', 'materjału'],
            ['się', 'śę'],
            ['nich', 'ńih'],
            ['przez', 'pszez'],
            ['rzeczywiście', 'żeczywiśće'],
            ['tych', 'tyh'],
            ['powstrzymujemy', 'powstszymujemy'],
            ['łyżka', 'łyżka'],
            ['trzy', 'tszy'],
            ['brzuch', 'bżuh'],
            ['buzi', 'buźi'],
            ['sito', 'śito'],
            ['cisza', 'ćisza'],
            ['zimno', 'źimno'],
            ['Chociaż', 'Hoćaż'],
            ['RZUT', 'ŻUT'],
            ['MANIFEST', 'MAŃIFEST'],
            ['rozpromienić', 'rozpromieńić']
        ];
    }

    /**
     * @dataProvider        textProvider
     */
    public function testFuturizeText($text, $futurizedText)
    {
        $futurus = new Futurus();
        $this->assertEquals($futurizedText, $futurus->futurizeText($text));

    }

    public function textProvider()
    {
        return [
            [
                "Umrzeć — tego nie robi się kotu.\r\nBo co ma począć kot\r\nw pustym mieszkaniu.",
                "Umżeć — tego ńe robi śę kotu.\r\nBo co ma począć kot\r\nw pustym mieszkańu."
            ],
        ];
    }
}
