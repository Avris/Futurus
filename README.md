## Avris Futurus ##

Narzędzie generuje na podstawie polskiego tekstu wejściowego tekst zapisany ortografią zgodną z Mańifestem Jasieńskiego.

Więcej informacji: [http://futurysci.avris.it](futurysci.avris.it)

    $futurus = new \Avris\Futurus\Futurus;
    
    echo $futurus->futurizeWord('wrzesień'); // wżeśeń
    
    echo $futurus->futurizeText('Umrzeć - tego nie robi się kotu'); // Umżeć - tego ńe robi śę kotu